CREATE TABLE Organisations (
  id INT NOT NULL PRIMARY KEY,
  nom TEXT NOT NULL,
  flag TEXT,
);
CREATE TABLE Scenarios (
  id INT NOT NULL PRIMARY KEY,
  nom TEXT NOT NULL,
  description TEXT NOT NULL,
);
CREATE TABLE Cartes (
  id INT NOT NULL PRIMARY KEY,
  nom TEXT NOT NULL,
  id_scenario INT NOT NULL,
);
CREATE TABLE Agents (
  id INT PRIMARY KEY,
  nom TEXT NOT NULL,
  role TEXT NOT NULL,
  icone TEXT NOT NULL,
  pourcentage_choisi REAL NOT NULL,
  id_organisations INT NOT NULL,
  id_carte_favorite INT,
  id_carte_handicape INT,
  CONSTRAINT fk_organisations FOREIGN KEY (id_organisations) REFERENCES Organisations(id),
  CONSTRAINT fk_carte_favorite FOREIGN KEY (id_carte_favorite) REFERENCES Cartes(id),
  CONSTRAINT fk_carte_handicape FOREIGN KEY (id_carte_handicape) REFERENCES Cartes(id)
);