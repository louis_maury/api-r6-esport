const express = require("express")
const Agents = require('./controllers/agents_controller')
const Organisations = require('./controllers/organisations_controller')
const Scenarios = require('./controllers/scenarios_controller')
const Cartes = require('./controllers/cartes_controller')
const Handlebars = require('handlebars')
const xpressHandlebars = require('express-handlebars')
const { allowInsecurePrototypeAccess } = require('@handlebars/allow-prototype-access')
const path = require('path')

const app = express()
const PORT = process.env.PORT || 8080

app.listen(PORT, () => {
    console.log('Serveur sur port :', PORT)
})

function logger(req, res, next) {
    console.log(new Date(), req.method, req.url);
    next();
}

app.use(logger)
app.get('/', (req, res, next) => {
    res.render('index')
})

// Handlebars
app.engine('handlebars', xpressHandlebars({
    handlebars: allowInsecurePrototypeAccess(Handlebars),
    defaultLayout: 'main'
}))
app.set('view engine', 'handlebars')

// Set static folde
app.use(express.static(path.join(__dirname, 'public')))

// Agents
app.get('/agents', Agents.all_agents)
app.get('/agents/nom=:nom', Agents.find_by_name)
app.get('/agents/id=:id', Agents.find_by_id)

// Organisations
app.get('/organisations', Organisations.all_organisations)
app.get('/organisations/nom=:nom', Organisations.find_by_name)
app.get('/organisations/id=:id', Organisations.find_by_id)


// Scenarios
app.get('/scenarios', Scenarios.all_scenarios)
app.get('/scenarios/nom=:nom', Scenarios.find_by_name)
app.get('/scenarios/id=:id', Scenarios.find_by_id)


// Cartes
app.get('/cartes', Cartes.all_cartes)
app.get('/cartes/nom=:nom', Cartes.find_by_name)
app.get('/cartes/id=:id', Cartes.find_by_id)

// 404
app.use((req, res) => {
    res.status(404)
    res.end("Hey le bleu ! Il n'y a rien a voir ici.")
})
