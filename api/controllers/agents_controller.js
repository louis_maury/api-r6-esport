const db = require('../models')
const Agents = db.Agents
const Organisations = db.Organisations
const Cartes = db.Cartes
const Op = db.Sequelize.Op

exports.find_by_name = (req, res) => {
    const name = req.params.nom

    Agents
        .findAll({
            include: [
                {
                    model: Organisations,
                },
                {
                    model: Cartes,
                    as: 'fav_carte'
                },
                {
                    model: Cartes,
                    as: 'handi_carte'
                }
            ],
            where: {
                nom: {
                    [Op.like]: `%${name}%`
                }
            }
        })
        .then(agents => {
            res.render('agents', {
                agent: agents
            })
        })
};

exports.find_by_id = (req, res) => {
    const id_getted = parseInt(req.params.id, 10)

    Agents
        .findAll({
            include: [
                {
                    model: Organisations,
                },
                {
                    model: Cartes,
                    as: 'fav_carte'
                },
                {
                    model: Cartes,
                    as: 'handi_carte'
                }
            ],
            where: {
                id: {
                    [Op.eq]: id_getted
                }
            }
        })
        .then(agents => {
            res.render('agents', {
                agent: agents
            })
        })
};


Agents.hasOne(Organisations, { foreignKey: 'id' })
Agents.belongsTo(Organisations, { foreignKey: 'id_organisations' })
Agents.hasOne(Cartes, { foreignKey: 'id' })
Agents.belongsTo(Cartes, { foreignKey: 'id_carte_favorite' })
Agents.hasOne(Cartes, { foreignKey: 'id', as: 'fav_carte' })
Agents.belongsTo(Cartes, { foreignKey: 'id_carte_handicape', as: 'handi_carte' })

exports.all_agents = (req, res) => {
    Agents
        .findAll({
            include: [
                {
                    model: Organisations,
                },
                {
                    model: Cartes,
                    as: 'fav_carte'
                },
                {
                    model: Cartes,
                    as: 'handi_carte'
                }
            ],
        })
        .then(agents => {
            res.render('agents', {
                agent: agents
            })
        })
}