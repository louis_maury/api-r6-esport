const db = require('../models')
const Cartes = db.Cartes
const Scenarios = db.Scenarios
const Op = db.Sequelize.Op

exports.find_by_name = (req, res) => {
    const name = req.params.nom

    Cartes
        .findAll({
            include: [{
                model: Scenarios,
                required: true
            }],
            where: {
                nom: {
                    [Op.like]: `%${name}%`
                }
            }
        })
        .then(cartes => {
            res.render('cartes', {
                carte: cartes
            })
        })
}

exports.find_by_id = (req, res) => {
    const id_getted = parseInt(req.params.id, 10)

    Cartes
        .findAll({
            include: [{
                model: Scenarios,
                required: true
            }],
            where: {
                id: {
                    [Op.eq]: id_getted
                }
            }
        })
        .then(cartes => {
            res.render('cartes', {
                carte: cartes
            })
        })
};

Cartes.hasOne(Scenarios, { foreignKey: 'id' })
Cartes.belongsTo(Scenarios, { foreignKey: 'id_scenario' })

exports.all_cartes = (req, res) => {
    Cartes
        .findAll({
            include: [{
                model: Scenarios,
                required: true
            }]
        })
        .then(cartes => {
            res.render('cartes', {
                carte: cartes
            })
        })
}
