const db = require('../models')
const Organisations = db.Organisations
const Op = db.Sequelize.Op

exports.find_by_name = (req, res) => {
    const name = req.params.nom

    Organisations
        .findAll({
            where: {
                nom: {
                    [Op.like]: `%${name}%`
                }
            }
        })
        .then(organisations => {
            res.render('organisations', {
                organisation: organisations
            })
        })
}

exports.find_by_id = (req, res) => {
    const id_getted = parseInt(req.params.id, 10)

    Organisations
        .findAll({
            where: {
                id: {
                    [Op.eq]: id_getted
                }
            }
        })
        .then(organisations => {
            res.render('organisations', {
                organisation: organisations
            })
        })
};

exports.all_organisations = (req, res) => {
    Organisations
        .findAll()
        .then(organisations => {
            res.render('organisations', {
                organisation: organisations
            })
        })
}

