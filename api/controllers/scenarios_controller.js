const db = require('../models')
const Scenarios = db.Scenarios
const Op = db.Sequelize.Op

exports.find_by_name = (req, res) => {
    const name = req.params.nom

    Scenarios
        .findAll({
            where: {
                nom: {
                    [Op.like]: `%${name}%`
                }
            }
        })
        .then(scenarios => {
            res.render('scenarios', {
                scenario: scenarios
            })
        })
}

exports.find_by_id = (req, res) => {
    const id_getted = parseInt(req.params.id, 10)

    Scenarios
        .findAll({
            where: {
                id: {
                    [Op.eq]: id_getted
                }
            }
        })
        .then(scenarios => {
            res.render('scenarios', {
                scenario: scenarios
            })
        })
};

exports.all_scenarios = (req, res) => {
    Scenarios
        .findAll()
        .then(scenarios => {
            res.render('scenarios', {
                scenario: scenarios
            })
        })
}
