const config = require('../config.json')
const Sequelize = require("sequelize")
const sequelize = new Sequelize({
    storage: config.database,
    dialect: config.dialect,
    host: config.host
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.Agents = require('./Agents')
db.Organisations = require('./Organisations')
db.Scenarios = require('./Scenarios')
db.Cartes = require('./Cartes')


module.exports = db