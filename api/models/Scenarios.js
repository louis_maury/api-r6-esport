const config = require('../config.json')
const db_file = config.database
const { Sequelize, Model, DataTypes } = require('sequelize')
const sequelize = new Sequelize({
    host: 'localhost',
    dialect: 'sqlite',
    storage: db_file
})

class Scenarios extends Model { }
Scenarios.init({
    id: {
        type: DataTypes.INTEGER, primaryKey: true
    },
    nom: DataTypes.TEXT,
    description: DataTypes.TEXT
}, { sequelize, modelName: 'Scenarios', timestamps: false })


module.exports = Scenarios