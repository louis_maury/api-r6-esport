const config = require('../config.json')
const db_file = config.database
const { Sequelize, Model, DataTypes } = require('sequelize')
const Organisations = require('./Organisations')
const Cartes = require('./Cartes')

const sequelize = new Sequelize({
    host: 'localhost',
    dialect: 'sqlite',
    storage: db_file
})

class Agents extends Model { }
Agents.init({
    id: {
        type: DataTypes.INTEGER, primaryKey: true
    },
    nom: DataTypes.TEXT,
    role: DataTypes.TEXT,
    icone: DataTypes.TEXT,
    pourcentage_choisi: DataTypes.REAL,
    id_organisations: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: Organisations,
            key: 'id_organisations'
        }
    },
    id_carte_favorite: {
        type: DataTypes.INTEGER,
        references: {
            model: Cartes,
            key: 'id_carte_favorite'
        }
    },
    id_carte_handicape: {
        type: DataTypes.INTEGER,
        references: {
            model: Cartes,
            key: 'id_carte_handicape'
        }
    }
}, { sequelize, modelName: 'Agents', timestamps: false })


module.exports = Agents
