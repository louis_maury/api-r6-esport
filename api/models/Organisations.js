const config = require('../config.json')
const db_file = config.database
const { Sequelize, Model, DataTypes } = require('sequelize')
const sequelize = new Sequelize({
    host: 'localhost',
    dialect: 'sqlite',
    storage: db_file
})

class Organisations extends Model { }
Organisations.init({
    id: {
        type: DataTypes.INTEGER, primaryKey: true
    },
    nom: DataTypes.TEXT,
    flag: DataTypes.TEXT
}, { sequelize, modelName: 'Organisations', timestamps: false })


module.exports = Organisations