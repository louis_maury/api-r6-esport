const config = require('../config.json')
const db_file = config.database
const { Sequelize, Model, DataTypes } = require('sequelize')
const sequelize = new Sequelize({
    host: 'localhost',
    dialect: 'sqlite',
    storage: db_file
})


class Cartes extends Model { }
Cartes.init({
    id: {
        type: DataTypes.INTEGER, primaryKey: true
    },
    nom: DataTypes.TEXT,
    id_scenario: {
        type: DataTypes.INTEGER,
        references: {
            model: 'Scenarios',
            key: 'id_scenario'
        }
    }
}, { sequelize, modelName: 'Cartes', timestamps: false })

module.exports = Cartes